const jwt=require('jsonwebtoken');

module.exports=(req,res,next)=>{
    const authHeader=req.get('Authorization');//req.get return http header field
    if (!authHeader) {
       req.isAuth=false;
       return next();
    }

    const token=authHeader.split(' ')[1];//this is the token
    let decodedToken;
    try{
        decodedToken=jwt.verify(token,'secret');
    }catch(err){
       req.isAuth=false;
       return next();
    }
    if (!decodedToken) {
        req.isAuth=false;
        return next();
    }
    req.userId=decodedToken.userId;//we are storing userId in the token
    req.isAuth=true;
    next();
}